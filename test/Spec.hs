module Spec where

import qualified Data.HashSet.InsOrd as OSet
import Data.Set (Set)
import qualified Data.Set as Set

prop_hashSetInsOrd :: Set Int -> Bool
prop_hashSetInsOrd xs =
    Set.toList xs == (OSet.toList . OSet.fromList . Set.toList $ xs)
