* haskell-sandbox
This project was generated using [[https://gitlab.com/joncol/hs-template][hs-template]].

* Building and running
To build and run the project, use:
#+begin_src bash
nix run .#haskell-sandbox:exe:haskell-sandbox-exe
#+end_src

To only build the project, use:
#+begin_src bash
nix build
#+end_src

To show all Flake outputs, you can use:
#+begin_src bash
nix flake show --option allow-import-from-derivation true
#+end_src
