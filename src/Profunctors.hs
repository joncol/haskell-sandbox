module Profunctors where

import Data.Char (toUpper)
import Data.Functor.Contravariant (Contravariant (..), Predicate (..))
import Data.List (intersperse)
import Data.Profunctor (dimap)

preProcess :: Int -> String
preProcess = ("int: " <>) . show

postProcess :: String -> String
postProcess = (<> ": post-processed") . map toUpper

f :: Int -> String
f = dimap preProcess postProcess (intersperse '_')

veryOdd :: Predicate Integer
veryOdd = contramap (`div` 2) (Predicate odd)

g :: [Bool]
g = getPredicate veryOdd <$> [0 .. 11]

-- [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
-- [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5]
-- [False, False, True, True, False, False, True, True, False, False, True, True]

g' :: [Bool]
g' = map (odd . (`div` 2)) [(0 :: Int) .. 11]
