module SeqMaybe where

f :: Int -> Maybe String
f 123 = Just "onetwothree"
f _ = Nothing
