{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}

module Lib where

import Control.Arrow ((&&&))
import Control.Monad (replicateM)
import Control.Monad.Reader (runReader)
import Control.Monad.Reader.Class (MonadReader, ask)
import Data.Foldable (asum)
import Data.List (groupBy, sortOn)
import Data.Text (Text)
import Data.Time (UTCTime (..))
import Data.Word (Word8)
import GHC.Generics (Generic)
import Optics ((%~), (<&>))
import Test.QuickCheck (generate, oneof, resize)
import Test.QuickCheck.Arbitrary.Generic
  ( Arbitrary (arbitrary)
  , GenericArbitrary (GenericArbitrary)
  )
import Test.QuickCheck.Gen (Gen)
import Test.QuickCheck.Instances.Time ()

newtype MyEnv = MyEnv {token :: Text}

-- | This function is called by `someFunc`.
foo :: MonadReader MyEnv m => m Int
foo = do
  MyEnv {token} <- ask
  pure $ case token of
    "foo" -> 42
    "bar" -> 123
    _ -> (-1)

-- | This function calls `foo`.
someFunc :: IO ()
someFunc = do
  let s = asum [Nothing, Just (1 :: Int), Just 2, Nothing]
  putStrLn $ "asum: " <> show s
  putStrLn "hello from `someFunc`"
  let x = runReader foo (MyEnv "foo")
  let y = runReader foo (MyEnv "bar")
  let z = runReader foo (MyEnv "baz")
  putStrLn $ "runReader foo (MyEnv \"foo\"): " <> show x
  putStrLn $ "runReader foo (MyEnv \"bar\"): " <> show y
  putStrLn $ "runReader foo (MyEnv \"baz\"): " <> show z

newtype Email = Email String
  deriving stock (Eq, Ord, Show)

instance Arbitrary Email where
  arbitrary =
    oneof $
      fmap
        (pure . Email)
        ["foo@example.com", "bar@example.com", "baz@example.com"]

data ChargeableItem = X | Y | Z
  deriving stock (Eq, Ord, Show, Enum, Generic)
  deriving (Arbitrary) via GenericArbitrary ChargeableItem

{- | Intermediate representation of a usage statistic (the quantity
 corresponding to a specific 'ChargeableItem'), as queried from the database.
-}
data UsageStat = UsageStat
  { timeWindowStart' :: UTCTime
  , userEmail' :: Email
  , chargeableItem :: ChargeableItem
  , quantity :: Word8
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving (Arbitrary) via GenericArbitrary UsageStat

-- | Usage stats per time period (day or month) and per user.
data UsageStats = UsageStats
  { timeWindowStart :: UTCTime
  , userEmail :: Email -- TODO: Is there an email and username for user groups?
  , stats :: [(ChargeableItem, Integer)]
  }
  deriving stock (Eq, Show, Generic)
  deriving (Arbitrary) via GenericArbitrary UsageStats

{- | Sample some 'UsageStat' items, and sort them by 'timeWindowStart' and
 'userEmail'.
-}
sampleUsageStats :: IO [UsageStat]
sampleUsageStats = do
  xs <- mySample (arbitrary :: Gen UsageStat) 20
  let timesTruncated = xs <&> #timeWindowStart' %~ truncTime
  pure $ sortOn (timeWindowStart' &&& userEmail') timesTruncated

mySample :: Gen a -> Int -> IO [a]
mySample g n = generate (replicateM n (resize 10 g))

truncTime :: UTCTime -> UTCTime
truncTime t = t {utctDayTime = 0}

{- | Collect "flat" stats into "buckets" based on time period and user email.
 The time period is either day or month.
 collectStats :: [UsageStat] -> [UsageStats]
-}
collectStats :: [UsageStat] -> [[UsageStats]]
collectStats us = map collect grouped
  where
    -- collectStats us = grouped

    -- Note that `grouped` contains buckets (lists) within which the timestamp
    -- and email are identical.
    grouped = groupBy (\x y -> sortKey x == sortKey y) us

    sortKey = timeWindowStart' &&& userEmail' &&& chargeableItem

    collect [] = []
    collect usageStats@(hd : _) =
      map
        (\_us -> UsageStats tws email [])
        usageStats
      where
        tws = timeWindowStart' hd
        email = userEmail' hd
        _ci = chargeableItem hd

{- | By using `let`, we can extract a `Maybe` value and apply a function to it,
 even if we're not "in" the `Maybe` monad, but in the `IO` monad.
-}
bindMaybeValueInIOMonad :: Maybe Int -> IO ()
bindMaybeValueInIOMonad mx = do
  let y = mx >>= \x -> pure $ x + 2
  putStrLn $ "y: " <> show y

  -- The above is equivalent to:
  let z = do
        x <- mx
        pure $ x + 2
  putStrLn $ "z: " <> show z

  pure ()

-- Note that the following does not type check:
-- mx >>= \x -> print $ x + 2

exeOrder :: IO ()
exeOrder = do
  putStrLn "What's your name?"
  name <- getLine
  error "Something goes bad here" -- Input is still asked for.
  putStrLn $ "Hello there, " <> name
