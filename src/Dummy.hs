{- | Dummy
 Foo
-}
module Dummy (foo) where

{- | Foo, a really useful function.
 Hello world
 Bar
-}
foo :: String
foo = "foo"
