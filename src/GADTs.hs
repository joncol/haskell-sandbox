{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}

module GADTs where

data User = Teacher | Student deriving (Show)

data Subject (users :: [User]) where
  Math :: Subject '[Teacher, Student]
  English :: Subject '[Teacher, Student]
  French :: Subject '[Teacher]

-- | This function works for both types of users.
foo :: Subject '[Teacher, Student] -> IO ()
foo Math = putStrLn "TS / math"
foo English = putStrLn "TS / english"

-- foo French  = putStrLn "TS / french" -- leads to inaccessible code warning, which is good

{- | This function works only for `Teacher`s.
TODO: Find a way to only require that the tag list /contains/ the type
      `Teacher`, not require that it is the only tag in the list.
-}
bar :: Subject '[Teacher] -> IO ()
bar Math = putStrLn "T / math" -- leads to inaccessible code warning, which is not good
bar English = putStrLn "T / english" -- leads to inaccessible code warning, which is not good
bar French = putStrLn "T / french"

baz :: IO ()
baz = do
  foo Math
  foo English
  -- foo French -- leads to type error, which is good
  -- bar Math -- leads to type error, which is not good
  -- bar English -- leads to type error, which is not good
  bar French
