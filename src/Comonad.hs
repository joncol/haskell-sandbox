{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE InstanceSigs #-}

module Comonad where

import Control.Comonad (Comonad (..))
import Data.List.NonEmpty (NonEmpty (..))
import Prelude hiding (cycle)

import qualified Data.Char as Char
import qualified Data.List.NonEmpty as NE

data Stream a = Cons a (Stream a) deriving (Functor)

instance Comonad Stream where
  extract :: Stream a -> a
  extract (Cons a _) = a

  duplicate :: Stream a -> Stream (Stream a)
  duplicate (Cons x xs) = (Cons (Cons x xs) (duplicate xs))

printStream :: Show a => Stream a -> Int -> IO ()
printStream s n = putStrLn $ show (take n $ toList s)

toList :: Stream a -> [a]
toList (Cons x xs) = x : toList xs

cycle :: NonEmpty a -> Stream a
cycle (x :| xs) = Cons x (cycle (NE.fromList (xs ++ [x])))

comonadTest :: IO ()
comonadTest = do
  let stream = cycle (NE.fromList ["foo" :: String, "bar", "baz"])
  printStream stream 10
  printStream (fmap (map Char.toUpper) stream) 10
  print $ extract stream
