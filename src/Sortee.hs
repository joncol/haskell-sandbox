module Sortee where

import Data.String.Sortee (between)

sorteeStuff :: IO ()
sorteeStuff = do
  print $ between "a" "b"
  print $ between "abc" "abcah"
  print $ between "abc" "abca"
  print $ between "abc" "abcb"
