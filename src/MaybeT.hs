{-# LANGUAGE ScopedTypeVariables #-}

module MaybeT where

import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Maybe

doSomeStuff :: MonadIO m => MaybeT m Int
doSomeStuff = do
  void . hoistMaybe $ Just (123 :: Int)
  liftIO $ putStrLn "A"
  void $ hoistMaybe Nothing -- Commenting this makes the computation continue.
  liftIO $ putStrLn "B"
  MaybeT $ do
    liftIO $ putStrLn "Doing stuff in MaybeT"
    pure $ Just 42

doFailingLetBind :: MonadIO m => MaybeT m ()
doFailingLetBind = do
  liftIO $ putStrLn "Trying failing let-bind in 'runMaybeT'"
  MaybeT $ do
    -- let Just (x :: Int) = Just 42
    let Just (x :: Int) = Nothing
    liftIO . putStrLn $ "x is: " <> show x
    pure $ Just ()

doSomeMaybeTStuff :: IO (Maybe Int)
doSomeMaybeTStuff = do
  putStrLn "-> doSomeMaybeTStuff"
  runMaybeT doSomeStuff

doSomeOtherMaybeTStuff :: IO (Maybe ())
doSomeOtherMaybeTStuff = do
  runMaybeT doFailingLetBind
