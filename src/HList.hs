{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}

-- See: https://www.youtube.com/watch?v=6snteFntvjM

module HList where

data HList tys where
  Nil :: HList '[]
  (:>) :: h -> HList t -> HList (h ': t)
infixr 5 :>

data Elem list elt where
  EZ :: Elem (x ': xs) x
  ES :: Elem xs x -> Elem (y ': xs) x

{-
>>> get (ES (ES (ES EZ))) (True :> "hello" :> [Just "a", Nothing] :> (42 :: Int) :> Nil)
42
-}
get :: Elem tys ty -> HList tys -> ty
get EZ (x :> _) = x
get (ES e) (_ :> xs) = get e xs
