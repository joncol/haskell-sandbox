import Lib

import Control.Monad (join)

import Comonad qualified
import Dummy qualified as D (foo)
import MaybeT qualified
import Profunctors qualified
import Sortee qualified

main :: IO ()
main = do
  print $ D.foo
  someFunc
  putStrLn "hello, from `main`"
  putStrLn "sequence"
  print $ sequence (Just (Just (Just (Nothing :: Maybe String))))
  print $ sequence (Just (Just (Nothing :: Maybe String)))
  print $ sequence (Just (Nothing :: Maybe String))
  putStrLn "join"
  print $ join (Just (Just "abc" :: Maybe String))
  print $ join (Just (Nothing :: Maybe String))
  print $ join (Nothing :: Maybe (Maybe String))
  -- putStrLn $ "f <$> Just 123: " <> show (SeqMaybe.f <$> Just 123)
  -- putStrLn $ "f <$> Just 456: " <> show (SeqMaybe.f <$> Just 456)
  -- putStrLn $ "f <$> Nothing: " <> show (SeqMaybe.f <$> Nothing)

  bindMaybeValueInIOMonad $ Just 123

  putStrLn $ "profunctor example 1:" <> show (Profunctors.f 123)
  putStrLn $ "profunctor example 2:" <> show Profunctors.g

  putStrLn "Running Comonad code..."
  Comonad.comonadTest

  Sortee.sorteeStuff

  -- exeOrder -- Throws exception, so commenting it out for now.

  MaybeT.doSomeMaybeTStuff >>= \x -> putStrLn $ "x: " <> show x
  MaybeT.doSomeOtherMaybeTStuff >>= \x -> putStrLn $ "x: " <> show x
